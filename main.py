import RpiMotorLib
from flask import Flask, render_template_string, redirect, request

# GPIO Pins
GPIO_pins = ("","","")  # This is the pins that the cables will connect to on the Raspberry Pi.

direction = ""  # This is the direction pin.
step = ""  # This is the step pin
distance = ""  # Default move is 1mm which is 80 steps

# Creating the motor object which can then be called within the code
motor_test = RpiMotorLib.A4988Nema(direction, step, GPIO_pins, "A4988Nema")

#Creating the flask web server
app = Flask(__name__)

# #HTML for the front end
html_code = '''
<html>
<head>

    <img src="platform_kinetics_logo.png" alt="Italian Trulli" width="500">
    <h1>Stepper Motor Controller</h1>
</head>
<body>
<div class="container">
    <div class="row" style = "padding:20">
        <div class="column1">
            <form method="POST" action="motor_up" class="center-block">
                <button type="submit" class="btn btn-primary">Up</button>
            </form>
        </div>
        <div class="column1">
            <form method="POST" action="motor_down" class="center-block">
                <button type="submit" class="btn btn-primary">Down</button>
            </form>
        </div>
    </div>
    <form method="POST" action="set_distance">
        <div class="btn-group" role="group" >
            <button name="distance" type="submit" class="btn btn-secondary" value="8">0.1 mm</button>
            <button name="distance" type="submit" class="btn btn-secondary" value="80">1 mm</button>
            <button name="distance" type="submit" class="btn btn-secondary" value="800">10 mm</button>
        </div>
    </form>

</div>
</body>
</html>

'''

#This method returns the home page, the source code is stored under the html_code variable
@app.route("/")
def home():
    return render_template_string(html_code)

#Distance method, returns the option that the user chooses to move the motor by
@app.route("/set_distance", methods=["POST"])
def set_distance():
    global distance
    global distance
    distance = int(request.form) # returns the vale of the variable on the sever side.
    return redirect(request.referrer)

#Moves the motor up by what the user chooses
@app.route("/motor_up", methods=["POST"]) #creates a new route that accepts a HTTP post method
def go_up():
    global distance
    motor_test.motor_go(False, "Full", distance, 0.01, False, .05)
    return redirect(request.referrer)

#Moves the motr down by what the user chooses
@app.route("/motor_down", methods=["POST"])
def go_down():
    global distance
    motor_test.motor_go(True, "Full", distance, 0.01, False, .05)
    return redirect(request.referrer)


# Run the application on a local server
if __name__ == "__main__":
    app.run()